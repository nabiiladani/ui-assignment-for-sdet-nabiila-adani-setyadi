import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo.midtrans.com/')

WebUI.click(findTestObject('Page_Sample Store/a_BUY NOW (2)'))

WebUI.delay(2)

WebUI.click(findTestObject('Page_Sample Store/div_CHECKOUT (2)'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Continue (7)'))

WebUI.delay(2)

WebUI.click(findTestObject('Object Repository/Page_Sample Store/div_Credit Card (7)'))

WebUI.delay(2)

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_sample-store-1582023755_cardnumber'), '4911 1111 1111 1113')

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input (8)'), '02 / 20')

WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_1 (8)'), '123')

WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Pay Now (7)'))

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Sample Store/input_Password_PaRes (7)'), '4tAN/DuJV7Y=')

WebUI.click(findTestObject('Object Repository/Page_Sample Store/button_OK (7)'))

WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Retry (1)'))

WebUI.closeBrowser()

