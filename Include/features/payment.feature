#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Credit Card Payment
  I want to use this template for Credit Card Payment

  @success
  Scenario Outline: User can use Credit Card for payment
    Given I navigate to midtrans pillow homepage
    When I click BUY NOW button
    And I checkout the shopping cart
    And I click CONTINUE button
    And I select CREDIT CARD for my payment
    And I enter valid Card Number <cardnumber> and Expiry Date <expirydate> and CVV <cvv>
    And I click PAY NOW button
    And I enter bank's otp <otpnumber>
    And I click OK button
    Then I have success transaction 

    Examples: 
      | cardnumber          | expirydate | cvv           | otpnumber | payment type |
      | 4811 1111 1111 1114 |     02/20  | tzH6RvlfSTg=  | 112233    | success      |
    
  @failed
  Scenario Outline: User can't use Credit Card for payment
    Given I navigate to midtrans pillow homepage
    When I click BUY NOW button
    And I checkout the shopping cart
    And I click CONTINUE button
    And I select CREDIT CARD for my payment
    And I enter invalid Card Number <cardnumber> and Expiry Date <expirydate> and CVV <cvv>
    And I click PAY NOW button
    And I enter bank's otp <otpnumber>
    And I click OK button
    Then I have failed transaction 

    Examples: 
      | cardnumber          | expirydate | cvv           | otpnumber | payment type |
      | 4911 1111 1111 1113 |     02/20  | tzH6RvlfSTg=  | 112233    | failed       |