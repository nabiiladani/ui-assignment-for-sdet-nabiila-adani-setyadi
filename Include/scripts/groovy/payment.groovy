import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class payment {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("I want to navigate midtrans pillow homepage to buy using credit card payment")
	def CCpayment() {
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://demo.midtrans.com/')
		WebUI.click(findTestObject('Page_Sample Store/a_BUY NOW'))
		WebUI.delay(2)
		WebUI.click(findTestObject('Page_Sample Store/div_CHECKOUT'))
		WebUI.delay(2)
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Continue (4)'))
		WebUI.delay(2)
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/div_Credit Card (4)'))
		WebUI.delay(2)
	}

	@When("I want to fill my credit card information")
	def fillCC(String cardnumber, expirydate, cvv) {
		println ("input credit card number: "+cardnumber)
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_sample-store-1582010540_cardnumber'), cardnumber)
		println ("input expiry date: "+expirydate)
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input (5)'), expirydate)
		println ("input cvv: "+cvv)
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_1 (5)'), cvv)
	}

	@And("I want to process the payment and navigate to bank's OTP")
	def pay() {
		println ("payment process")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Pay Now (4)'))
	}

	@And("I want to fill bank's OTP")
	def otp(String otpnumber){
		WebUI.setEncryptedText(findTestObject('Object Repository/Page_Sample Store/input_Password_PaRes (4)'), otpnumber)
	}

	@Then("I get a message transaction successfull")
	def transactionIsSuccess(){
		println("Your Transaction is success")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Done (3)'))
		WebUI.closeBrowser()
	}
	@Then("I get a message transaction failed")
	def transactionIsFailed(){
		println("Your Transaction is failed")
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Retry'))
		WebUI.closeBrowser()
	}
}