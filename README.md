# UI ASSIGNMENT FOR SDET - Nabiila Adani Setyadi

This repository contains sample test cases using [Katalon Studio](https://www.katalon.com/).

# Getting started
*  [Check out the code from this repository](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository)
*  [Open](https://docs.katalon.com//display/KD/Manage+Test+Project) project from katalon studio
*  Choose the test cases, and run the project using [Firefox](https://www.mozilla.org/en-US/)


